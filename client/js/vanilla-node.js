// Required modules
var http = require("http");

// Create server instance
var app = http.createServer()

http.createServer(function(request, response) {
  var method = request.method;
  var url = request.url;
  var body = [];
  
  request.on('error', function(err) {
    console.error(err);
  }).on('data', function(chunk) {
    body.push(chunk);
  }).on('end', function() {
    body = Buffer.concat(body).toString();

    response.on('error', function(err) {
      console.error(err);
    });
    
    // A Route
    if (request.url == "/hello") {
      response.writeHead(200, { "Content-Type": "text/html" });
      response.end("This route works!");
    }

    response.writeHead(200, {'Content-Type': 'application/json'})

    var responseBody = {
      method: method,
      url: url,
      body: body
    };
    
    response.end("Hello World!\n")
    // response.end(JSON.stringify(responseBody))

    // END OF NEW STUFF
  });
}).listen(process.env.PORT, process.env.IP);

// app.get('/hello', function(req, res) {
//     res.writeHead(200, {"Content-Type": "text/html"});
//     res.end("Hello World!");
// });

// // Start server
// app.listen(1337, "localhost");
// console.log("Server running at http://localhost:1337/");