// Basic app
/*
  Whas all this stuff?
  
  1 Path is a module to help you modify file paths more easily
  2 Express is an application server framework.
    http://expressjs.com/en/guide/routing.html
    
  3 app is an instance of an Express server.
  4 Postgres is our database and pg is a client tool for connecting with it.
  
  5 Body Parser parsers request contet. We have to specify which content it
  should parse. In this case, we only want it parsing URL data and JSON.
  
  Cloud9 has limitations and odd quirks. If something appears to go wrong, try
  the code on your local setup to rule out C9 issues so you don't waste time and
  frustration figuring out something that's not broken!
*/
var path      = require('path'),
    express   = require('express'),
    app       = express(),
    port      = process.env.PORT,
    host      = process.env.IP,
    pg        = require('pg'),
    bodyParser= require('body-parser'),
    db        = require('./config.js');

// var routes= require('./routes/index');

// template engine and views
app.set('views', path.join(__dirname, 'client/views'));
app.set('view engine', 'jade');
app.use(express.static(__dirname + 'client'));

// Parse request body
// http://bit.ly/learn-about-body-parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({'extended':true}));

/* ///////////////////////////////////////////////////////////////////
ROUTES! 
  This is how users get around your app. Note the app.METHOD
  pattern below. 
  
  `app` references our instance of express() and we can call
  an HTTP method upon it to route uers. See Express documentation
  for a good explanation.
  
*/
app.get("/", function(req,res) {
  // This send method just prints whatever you want on the webpage
  // for your visitors. This can be a full-on HTML string or just
  // a little message. Be creative!
  res.send("Welcome to my app!");
});

app.get("/hello", function(req, res) {
  // The render method will fetch a template from your views and
  // show that to the users when they visit this endpoint.
  res.render("ejs/index.ejs");
});

// Show all the users
app.get("/hackers", function(req, res) {
  db.query('SELECT * FROM hackers', function(err, dbResponse) {
    if (!err) {
      res.render('ejs/list.ejs', { hackers: dbResponse.rows });
    }
  });
});

// Add a user
app.get("/new", function(req, res) {
  res.render('ejs/new.ejs');
});

app.post("/hackers", function(req, res) {
  var params = [req.body.name, req.body.email, req.body.password];
  db.query('INSERT INTO hackers (name, email, password) VALUES ($1, $2, $3)', params, function(err, dbResponse) {
    if (!err) {
      res.render('ejs/list.ejs', { hackers: dbResponse.rows });
    }
  });
});

// Show a user
app.get("/hackers/:id", function(req, res) {
  var userId = [req.params.id];
  db.query('SELECT * FROM hackers WHERE id = $1', userId, function(err, dbResponse) {
    console.log(dbResponse.rows[0])
    if (!err) {
      res.render('ejs/detail.ejs', { hacker: dbResponse.rows[0] });
    }
  });
});
//////////////-END ROUTES-//////////////////////////////////////////

// server
app.listen(port, host, function() {
  console.log('Server running on %s', port);
  console.log(path.join(__dirname));
});

