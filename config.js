// SETUP DB
var pg = require('pg');

// DB Configuration
// https://github.com/brianc/node-postgres/wiki/pg
// https://community.c9.io/t/setting-up-postgresql/1573
var db = {
    config:  {
      database: "test",
      port: 5432,
      user: 'postgres',
      password: 'test',
      host: process.env.IP
    },
    connect: function(callback) {
      pg.connect(db.config, function(err, client, done){
        if (err) {
          console.error("There's been a problem ...", err);
        }
        callback(client);
        done();
      });
    },
    query: function(statement, params, callback){
      db.connect(function(client){
        client.query(statement, params, callback);
      });
    }
};

module.exports = db;
